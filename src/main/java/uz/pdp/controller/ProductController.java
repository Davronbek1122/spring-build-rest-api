package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.entity.Brand;
import uz.pdp.entity.Category;
import uz.pdp.payload.BrandDto;
import uz.pdp.payload.CategoryDto;
import uz.pdp.payload.ProductDto;
import uz.pdp.service.BrandService;
import uz.pdp.service.CategoryService;
import uz.pdp.service.ProductService;

import java.util.List;

@Controller
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    BrandService brandService;


    @GetMapping
    public String getAll(Model model){
        List<ProductDto> all = productService.getAll();
        model.addAttribute("productList", all);
        return "showProducts";
    }

    @GetMapping("/product/addProduct")
    public String addBookPage(Model model){
        List<CategoryDto> categoryDtoList = categoryService.getAll();
        List<Brand> brandList = brandService.getAll();
        model.addAttribute("brands",brandList);
        model.addAttribute("categories",categoryDtoList);
        return "addBookPage";
    }

    @PostMapping("/product/save")
    public String saveBook(@RequestParam String name,
                           @RequestParam Double price,
                           @RequestParam Integer category,
                           @RequestParam Integer brand,
                           Model model){
        List<Category> categories = categoryService.getById(category);
        Category category1 = categories.get(0);
        List<Brand> byId = brandService.getById(brand);
        Brand brand1 = byId.get(0);
        ProductDto productDto=new ProductDto();
        productDto.setName(name);
        productDto.setPrice(price);
        productDto.setCategory(category1);
        productDto.setBrand(brand1);
        productService.save(productDto);


        List<ProductDto> all = productService.getAll();
        model.addAttribute("productList", all);
        return "showProducts";
    }

    @GetMapping("/product/edit/{id}")
    public String edit(@PathVariable Integer id,
                       Model model){
        List<ProductDto> byId = productService.getById(id);
        ProductDto productDto = byId.get(0);
        List<CategoryDto> categoryDtoList = categoryService.getAll();
        List<Brand> brandList = brandService.getAll();
        model.addAttribute("brands",brandList);
        model.addAttribute("categories",categoryDtoList);
        model.addAttribute("product",productDto);
        return "editPage";
    }

    @PostMapping("/product/editSave/{id}")
    public String editSave(@PathVariable Integer id,
                           @RequestParam String name,
                           @RequestParam Double price,
                           @RequestParam Integer category,
                           @RequestParam Integer brand,
                           Model model){
        List<ProductDto> byId1 = productService.getById(id);
        ProductDto productDto = byId1.get(0);

        List<Category> categories = categoryService.getById(category);
        Category category1 = categories.get(0);
        List<Brand> byId = brandService.getById(brand);
        Brand brand1 = byId.get(0);
        productDto.setName(name);
        productDto.setPrice(price);
        productDto.setCategory(category1);
        productDto.setBrand(brand1);

        productService.editProduct(productDto);


        List<ProductDto> all = productService.getAll();
        model.addAttribute("productList", all);
        return "showProducts";
    }
}
