package uz.pdp.dbConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnection {
    private static final String DB_URL="jdbc:postgresql://localhost:5432/for_jpa";
    private static final String DB_USERNAME="postgres";
    private static final String DB_PASSWORD="root123";

    public static Connection createConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
    }

//    public static List<Book> getBookList() throws SQLException, ClassNotFoundException {
//        Connection connection =createConnection();
//        ResultSet resultSet = connection.createStatement().executeQuery("select * from book;");
//        List<Book> bookList = new ArrayList<>();
//        while (resultSet.next()){
//            Book book = new Book();
//            book.setId(resultSet.getInt(1));
//            book.setName(resultSet.getString(2));
//            book.setAuthor(resultSet.getString(3));
//            book.setPrice(resultSet.getDouble(4));
//            bookList.add(book);
//        }
//        return bookList;
//    }
//
//    public static Book getBookById(int parseInt) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        ResultSet resultSet = connection.createStatement().executeQuery("select * from book where id="+parseInt+";");
//        Book book =null;
//        while (resultSet.next()){
//            book=new Book();
//            book.setId(resultSet.getInt(1));
//            book.setName(resultSet.getString(2));
//            book.setAuthor(resultSet.getString(3));
//            book.setPrice(resultSet.getDouble(4));
//        }
//        return book;
//    }
//
//    public static void saveOrEdit(String id, String name) throws SQLException, ClassNotFoundException {
//        String query="";
//        if (id != null){
//            query="update book set name='"+name+"' where id ="+Integer.parseInt(id)+";";
//        }else {
//            query="insert into book(name,action) values ('"+name+"','INSERT');";
//        }
//        Connection connection = createConnection();
//        Statement statement = connection.createStatement();
//        statement.execute(query);
//    }
//
//    public static void remove(int id) throws SQLException, ClassNotFoundException {
//        Connection connection = DBConnection.createConnection();
//        Statement statement = connection.createStatement();
//        statement.execute("delete from book where id="+id+";");
//    }
//
//    public static void saveBook(Book book) throws SQLException, ClassNotFoundException {
//        Connection connection = DBConnection.createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement("insert into book(name, author,price) values(?,?,?);");
//        preparedStatement.setString(1, book.getName());
//        preparedStatement.setString(2, book.getAuthor());
//        preparedStatement.setDouble(3,book.getPrice());
//
//        preparedStatement.execute();
//    }
//
//    public static void editBook(Book book) throws SQLException, ClassNotFoundException {
//        Connection connection = DBConnection.createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement("update book set name=?, author=?, price=? where id="+book.getId()+";");
//        preparedStatement.setString(1, book.getName());
//        preparedStatement.setString(2, book.getAuthor());
//        preparedStatement.setDouble(3,book.getPrice());
//
//        preparedStatement.execute();
//    }


//    public static Users getUserByPassUserName(String username, String password) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement("select * from users where username='" + username + "' and password='" + password + "' ;");
//        ResultSet resultSet = preparedStatement.executeQuery();
//
//        Users users=new Users();
//        while (resultSet.next()){
//            users.setId(resultSet.getInt(1));
//            users.setUsername(resultSet.getString(2));
//            users.setPassword(resultSet.getString(3));
//            users.setRole_id(resultSet.getInt(4));
//        }
//        return users;
//    }
//
//    public static Users getUserById(Integer userId) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement("select * from users where id='" + userId + "';");
//        ResultSet resultSet = preparedStatement.executeQuery();
//
//        Users users=new Users();
//        while (resultSet.next()){
//            users.setId(resultSet.getInt(1));
//            users.setUsername(resultSet.getString(2));
//            users.setPassword(resultSet.getString(3));
//            users.setRole_id(resultSet.getInt(4));
//        }
//        return users;
//    }

//    public static List<Book> getBookListByUserId(int userId) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement(
//                "select id,name from book EXCEPT " +
//                        "select b.id,b.name from user_vs_book ub " +
//                "join users u on u.id=" + userId + " and u.id=ub.user_id" +
//                " join book b on b.id=ub.book_id");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        List<Book> books=new ArrayList<>();
//        while (resultSet.next()){
//            int id = resultSet.getInt(1);
//            String name = resultSet.getString(2);
//            Book book=new Book(id,name);
//            books.add(book);
//        }
//        return books;
//    }
//    public static List<Book> getUnreadBookListByUserId(int userId) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement(
//                "select b.id,b.name from user_vs_book ub " +
//                        "join users u on u.id=" + userId + " and u.id=ub.user_id" +
//                        " join book b on b.id=ub.book_id");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        List<Book> books=new ArrayList<>();
//        while (resultSet.next()){
//            int id = resultSet.getInt(1);
//            String name = resultSet.getString(2);
//            Book book=new Book(id,name);
//            books.add(book);
//        }
//        return books;
//    }
}
