package uz.pdp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uz.pdp.entity.Brand;
import uz.pdp.entity.Category;
import uz.pdp.utills.CommonUtills;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class BrandRepo {
    @Autowired
    CommonUtills commonUtills;
    public Brand getBrandById(Integer id) {
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select * from brand where id=" + id + ";");
                ResultSet resultSet = preparedStatement.executeQuery();
                Brand brand = new Brand();
                while (resultSet.next()) {
                    brand.setId(resultSet.getInt(1));
                    brand.setName(resultSet.getString(2));
                }
                return brand;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public List<Brand> getAll() {
        List<Brand> brandList =new ArrayList<>();
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select * from brand");
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Brand brand = new Brand();
                    brand.setId(resultSet.getInt(1));
                    brand.setName(resultSet.getString(2));
                    brandList.add(brand);
                }

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return brandList;
    }
}
