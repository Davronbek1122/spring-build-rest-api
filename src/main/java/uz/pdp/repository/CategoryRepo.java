package uz.pdp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uz.pdp.entity.Category;
import uz.pdp.payload.CategoryDto;
import uz.pdp.utills.CommonUtills;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryRepo {

    @Autowired
    CommonUtills commonUtills;
    public Category getCategoryById(Integer id) {
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select * from category where id=" + id + ";");
                ResultSet resultSet = preparedStatement.executeQuery();
                Category category = new Category();
                while (resultSet.next()) {
                    category.setId(resultSet.getInt(1));
                    category.setName(resultSet.getString(2));
                }
                return category;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public List<CategoryDto> getAll(){
        List<CategoryDto> categoryList=new ArrayList<>();
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select * from category");
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    CategoryDto category = new CategoryDto();
                    category.setId(resultSet.getInt(1));
                    category.setName(resultSet.getString(2));
                    categoryList.add(category);
                }

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return categoryList;
    }
}
