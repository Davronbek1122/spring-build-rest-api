package uz.pdp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uz.pdp.entity.Brand;
import uz.pdp.entity.Category;
import uz.pdp.payload.ProductDto;
import uz.pdp.utills.CommonUtills;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductRepository {
    @Autowired
    private CommonUtills commonUtills;
    @Autowired
    CategoryRepo categoryRepo;

    @Autowired
    BrandRepo brandRepo;

    public List<ProductDto> getAll() {
        List<ProductDto> productDtoList = new ArrayList<>();
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select * from product;");
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    ProductDto productDto = new ProductDto();
                    productDto.setId(resultSet.getInt(1));
                    productDto.setName(resultSet.getString(2));
                    productDto.setPrice(resultSet.getDouble(3));
                    productDto.setCategory(categoryRepo.getCategoryById(resultSet.getInt(4)));
                    productDto.setBrand(brandRepo.getBrandById(resultSet.getInt(5)));
                    productDtoList.add(productDto);
                }

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return productDtoList;
    }

    public List<ProductDto> getById(Integer id) {
        List<ProductDto> productDtoList=new ArrayList<>();
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select * from product where id="+id+";");
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    ProductDto productDto = new ProductDto();
                    productDto.setId(resultSet.getInt(1));
                    productDto.setName(resultSet.getString(2));
                    productDto.setPrice(resultSet.getDouble(3));
                    productDto.setCategory(categoryRepo.getCategoryById(resultSet.getInt(4)));
                    productDto.setBrand(brandRepo.getBrandById(resultSet.getInt(5)));
                    productDtoList.add(productDto);
                }

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return productDtoList;
    }


    public void save(ProductDto productDto) {
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(
                        "insert into product (name,price,category_id,brand_id) values (?,?,?,?) ;");
                preparedStatement.setString(1,productDto.getName());
                preparedStatement.setDouble(2,productDto.getPrice());
                preparedStatement.setInt(3,productDto.getCategory().getId());
                preparedStatement.setInt(4,productDto.getBrand().getId());
                preparedStatement.execute();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void editProduct(ProductDto productDto) {
        Connection connection = commonUtills.getConnection();
        if (connection != null) {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(
                        "update product set name=?, price=?, category_id=?, brand_id=? where id=?");
                preparedStatement.setString(1,productDto.getName());
                preparedStatement.setDouble(2,productDto.getPrice());
                preparedStatement.setInt(3,productDto.getCategory().getId());
                preparedStatement.setInt(4,productDto.getBrand().getId());
                preparedStatement.setInt(5,productDto.getId());
                preparedStatement.execute();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
