package uz.pdp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.entity.Brand;
import uz.pdp.entity.Category;
import uz.pdp.payload.CategoryDto;
import uz.pdp.repository.BrandRepo;
import uz.pdp.repository.CategoryRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandService {
    @Autowired
    BrandRepo brandRepo;

    public List<Brand> getAll(){
        return brandRepo.getAll();
    }

    public List<Brand> getById(Integer id) {
        List<Brand> brandList=new ArrayList<>();
        brandList.add(brandRepo.getBrandById(id));
        return brandList;
    }
}
