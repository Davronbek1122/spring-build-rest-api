package uz.pdp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.entity.Category;
import uz.pdp.payload.CategoryDto;
import uz.pdp.payload.ProductDto;
import uz.pdp.repository.CategoryRepo;
import uz.pdp.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {
    @Autowired
    CategoryRepo categoryRepo;

    public List<CategoryDto> getAll(){
        return categoryRepo.getAll();
    }

    public List<Category> getById(Integer id) {
        List<Category> categoryRepos=new ArrayList<>();
        categoryRepos.add(categoryRepo.getCategoryById(id));
        return categoryRepos;
    }
}
