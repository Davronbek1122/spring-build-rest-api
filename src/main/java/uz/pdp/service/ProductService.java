package uz.pdp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.payload.ProductDto;
import uz.pdp.repository.ProductRepository;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductDto> getAll(){
        return productRepository.getAll();
    }

    public List<ProductDto> getById(Integer id) {
        return productRepository.getById(id);
    }

    public void save(ProductDto productDto) {
        productRepository.save(productDto);
    }

    public void editProduct(ProductDto productDto) {
        productRepository.editProduct(productDto);
    }
//
//    public List<ResBook> getByPageable(Integer page, Integer size) {
//        return productRepository.getMethod(null,page,size,false);
//    }
//
//    public List<ResBook> saveBook(ReqBook reqBook) {
//        productRepository.save(reqBook);
//        return productRepository.getMethod(null,null,null,true);
//    }
//
//    public List<ResBook> search(String search) {
//        return productRepository.search(search);
//    }
}
