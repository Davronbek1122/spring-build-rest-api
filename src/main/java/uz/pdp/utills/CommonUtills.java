package uz.pdp.utills;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class CommonUtills {
//    @Value("${db.url}")
    private String url="jdbc:postgresql://localhost:5432/for_jpa";

//    @Value("${db.username}")
    private String username="postgres";

//    @Value("${db.password}")
    private String password="root123";

    public Connection getConnection(){
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url,username,password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
